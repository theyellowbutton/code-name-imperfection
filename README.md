# Code Name Imperfection

> Il fatto che l'attività svolta in modo così imperfetto sia stata e sia tuttora per me una fonte inesauribile di gioia mi fa credere che l'imperfezione nello svolgere il compito che ci siamo prefissati, o che ci è stato assegnato, sia più consona alla natura umana così imperfetta che alla perfezione. Rita Levi-Montalcini

> The fact that the activity performed so imperfectly has been and still is for me an inexhaustible source of joy makes me believe that imperfection in accomplishing the task we set out to do, or have been assigned to do, is more in keeping with human nature so imperfect than perfection. Rita Levi-Montalcini (Translated from Italian)

## Purposes
The purpose of this project is to create a video game based on PAC-MAN with the aim of having fun and learning game development. Hence, all dynamics of this project can be observed and determined by playing the original video game. The development will focus only in creating and testing basic functionalities in order to have a full fledged level or a couple of them which are sufficient to demonstrate that all characteristics have been met. Although many functionalities can be achieved in different ways, some of them in a very sophisticated way, the creativity will led the development and will ensure to find a way at any cost (often by adopting a non elegant approach rather than give up).

## Environment and tools
Godot 4 Beta

GUT (currently not working for Godot Beta. It will be used when stable. When possible it will be used a TDD approach using scenes.)

## Disclaimer
PAC-MAN is a trademark of BANDAI NAMCO ENTERTAINMENT INC.
This project created just for fun and educational purposes with no profit in mind and it represents only a proof of concept. Hence it mustn't be sold for profit.
