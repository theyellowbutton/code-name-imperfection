extends Node2D

@onready var stats = $Stats
@onready var collectables_handler = $CollectablesHandler
@onready var character = $Character
	
func set_stats():
	if collectables_handler and collectables_handler.collectables_left != 0:
		stats.text = str(collectables_handler.collectables_left) + "/" + \
		str(collectables_handler.collectables) + " " + str(collectables_handler.random_collected)

func _on_collectables_handler_status_updated():
	set_stats()

func _on_collectables_handler_all_collected():
	stats.text = ":-)"
	character.speed_factor = 0
