extends Node2D

@onready var dr1 = $DynamicRail
@onready var dr2 = $DynamicRail2
@onready var dr3 = $DynamicRail3
@onready var dr4 = $DynamicRail4
@onready var dr5 = $DynamicRail5
@onready var dr6 = $DynamicRail6
@onready var dr7 = $DynamicRail7
@onready var dr8 = $DynamicRail8
@onready var dr9 = $DynamicRail9
@onready var dr10 = $DynamicRail10
@onready var dr11 = $DynamicRail11
@onready var dr12 = $DynamicRail12

@onready var timer = $Timer
@onready var timer2 = $Timer2

var change_dr4_path = true
var change_dr7_path = true
var change_dr9_path = true
var change_dr10_path = 2
var start_timer2 = true

func _ready():
	Engine.set_max_fps(60)
#	remove_child(dr1)
#	remove_child(dr2)
#	remove_child(dr3)
#	remove_child(dr4)
#	remove_child(dr5)
#	remove_child(dr6)
#	remove_child(dr7)
#	remove_child(dr8)
#	remove_child(dr9)
#	remove_child(dr10)
#	remove_child(dr11)
#	remove_child(dr12)
	dr6.go_ahead = true
	dr6.clear_paths()
	dr6.push_direction(Vector2.RIGHT)
	dr6.push_direction(Vector2.UP)
	dr6.make_rail()
	dr12.clear_paths()
	dr12.push_direction(Vector2.RIGHT)
	dr12.make_rail()
	pass


func _process(delta):
	
	if dr4.is_stopped() and change_dr4_path:
		change_dr4_path = false
		await get_tree().create_timer(2).timeout
		dr4.clear_paths()
		dr4.push_direction(Vector2.UP)
		dr4.make_rail()
	pass

func _on_timer_timeout():

	if change_dr7_path:
		change_dr7_path = false
		dr7.clear_paths()
		dr7.push_path(PackedVector2Array([Vector2(2304, 896), Vector2(2600, 896)]))
		dr7.push_path(PackedVector2Array([Vector2(2304, 896), Vector2(2304, 1024)]))
		dr7.make_rail()

	if change_dr9_path:
		change_dr9_path = false
		dr9.clear_paths()
		dr9.push_path(PackedVector2Array([Vector2(2560, 896), Vector2(2560, 1024)]))
		dr9.make_rail()

	if change_dr10_path > 0:
		change_dr10_path -= 1
		dr10.clear_paths()
		dr10.push_direction(Vector2.DOWN)
		dr10.make_rail()
		pass

	dr11.clear_paths()
	dr11.push_direction(Vector2.DOWN)
	dr11.make_rail()

	timer2.one_shot = 1
	timer2.start(1)
	pass


func _on_timer_2_timeout():
	dr11.clear_paths()
	dr11.push_direction(Vector2.UP)
	dr11.make_rail()
	pass


func _on_dynamic_rail_12_on_start():
	pass


func _on_dynamic_rail_12_on_stop():
	dr12.clear_paths()
	dr12.push_direction(Vector2.DOWN)
	dr12.push_direction(Vector2.LEFT)
	dr12.make_rail()
	pass


func _on_timer_3_timeout():
	dr5.clear_paths()
	dr5.push_direction(Vector2.LEFT)
	dr5.make_rail()
	await get_tree().create_timer(0.5).timeout
	dr5.clear_paths()
	dr5.push_direction(Vector2.LEFT)
	dr5.make_rail()
	await get_tree().create_timer(0.5).timeout
	dr5.clear_paths()
	dr5.push_direction(Vector2.LEFT)
	dr5.make_rail()
	await get_tree().create_timer(0.5).timeout
	pass
