extends Node2D

@onready var inky = $Inky
@onready var inky_controller = $Inky/Rail/Wagon/Passenger/EnemyController
@onready var line = $Line2D

func _process(delta):
	line.clear_points()
	line.add_point(inky_controller.get_target())
	line.add_point(inky.wagon_position())
