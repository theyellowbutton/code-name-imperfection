extends Node2D

var current_level = null
var level_handler = null
@onready var board = $Board

func _ready():
	level_handler = LevelsHandler.new()
	load_next_level()
	
func restart_current_level():
	call_deferred('_remove_level', current_level)
	var new_current_level = level_handler.get_level()
	call_deferred('_start_level', new_current_level)
	
func load_next_level():
	
	if current_level != null:
		call_deferred('_remove_level', current_level)
	
	collectables_album.clear()
	level_handler.fetch_next_level()
	var next_level = level_handler.get_level()
	call_deferred('_start_level', next_level)

func _remove_level(level):
	remove_child(level)
	level.queue_free()
	
func _start_level(level):
	connect_signals(level)
	add_child(level)
	level.start()
	current_level = level
	
func connect_signals(level):
	level.connect("game_over", _on_game_over)
	level.connect("on_collected", _on_collected)
	
func _on_game_over(won: bool):
	print("GAME OVER %s" % won)
	if won:
		board.next_level()
		load_next_level()
	else:
		if board.get_life():
			restart_current_level()
		else:
			print("END")
	
func _on_collected(what: Enums.CollectableType):
	board.calculate_and_add_points(what)

func _process(delta):
	pass
