extends Node

class_name LevelsHandler

var _level_paths = [
	'res://levels/playgrounds/level_playground/level_playground.tscn',
	'res://levels/playgrounds/next_level_playground/next_level_playground.tscn'
]

var _current_index = -1
	
func get_level():
	
	if _current_index < 0:
		_current_index = 0
		
	var l = _level_paths[_current_index]
	var scene = load(l).instantiate()
	return scene

func fetch_next_level():
	_current_index += 1
	
	if _current_index >= _level_paths.size():
		_current_index = 0
