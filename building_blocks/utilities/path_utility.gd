extends Object

class_name PathUtility

static func convert_path_to_curve(path: PackedVector2Array) -> Curve2D:
	var _curve = Curve2D.new()
	
	for point in path:
		_curve.add_point(point)
		
	return _curve
	
static func convert_curve_to_path(_curve: Curve2D, tile_map_hook: TileMapHook) -> PackedVector2Array:
	_curve.bake_interval = min(tile_map_hook.tile_size.x, tile_map_hook.tile_size.y)
	return _curve.get_baked_points()
	
# partially copied
static func adapt_path(path: PackedVector2Array, tile_map_hook: TileMapHook, jump: bool = false) -> PackedVector2Array:
	var previous_point = Vector2(-1,-1)
	var adapted_path = PackedVector2Array()
	
	for point in path:
		var new_point = tile_map_hook.hook_the_corner(point, jump)
		if new_point != Vector2(-1,-1) and new_point != previous_point:
			if previous_point == Vector2(-1,-1) or new_point.distance_to(previous_point) == min(tile_map_hook.tile_size.x, tile_map_hook.tile_size.y):
				previous_point = new_point
				adapted_path.append(new_point)
	
	return adapted_path
