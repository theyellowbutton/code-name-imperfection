extends Node

enum EnemyMode {
	CHASE, 
	SCATTERED, 
	FRIGHTENED,
	HIT,
	RECOVERING,
	LAUNCHING
}

enum EnemySpeedMode {
	NORMAL,
	SLOW,
	FAST,
}

enum EnemyBehaviour {
	BLINKY,
	PINKY,
	INKY,
	CLYDE
}

enum CollectableType {
	coin,
	energizer,
	bonus,
	enemy
}
