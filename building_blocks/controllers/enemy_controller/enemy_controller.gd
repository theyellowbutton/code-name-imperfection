extends Node2D
class_name EnemyController

@onready var dynamic_rail = get_parent().get_parent()
@onready var agent: NavigationAgent2D = $NavigationAgent2D
@onready var move_timer: Timer = $MoveTimer
@onready var frightened_timer: Timer = $FrightenedTimer
@onready var recovery_timer: Timer = $RecoveryTimer

@onready var _tmhook: TileMapHook = dynamic_rail.get_node("TileMapHook")
@onready var _scattering_curve: Path2D = null

@export var behaviour: Enums.EnemyBehaviour = 0
@export var debug_line: Line2D = null
@export var refresh_rate: float = 1
@export var auto_refresh_rate: bool = true
@export var chase_scatted_interval: int = 30
@export var chase_ratio: float = 0.5
@export var recovery_time: float = 5
@export var frightened_time: float = 10
@export var slow_factor: float = 2
@export var fast_factor: float = 4
@export var start_recoverying: bool = true
@export var first_recovery_time: float = 1


var _killable = false
var _target_changed = false
var _current_mode: Enums.EnemyMode
var _current_mode_id: int = 0
var _points_to_target = null
var _scattering_path = null
var _setup_scattered = false
var _setup_random = false
var _frightened = false
var _random_number_generator = RandomNumberGenerator.new()
var _init_position = null
var _init_speed_factor = null
var _preparing_scattering = true
var _close_to_offset_target = false

func _ready():
	_random_number_generator.randomize()
	_init_position = global_position
	_init_speed_factor = dynamic_rail.speed_factor
	if auto_refresh_rate:
		refresh_rate = (1 / dynamic_rail.speed_factor) - 0.1
	
	_scattering_curve = dynamic_rail.get_node_or_null("ScatteringPath")
	
	if _scattering_curve != null:
		var path = PathUtility.convert_curve_to_path(_scattering_curve.get_curve(), _tmhook)
		path = path * Transform2D(0, dynamic_rail.position * -1)
		_scattering_path = PathUtility.adapt_path(path, _tmhook)
		
	add_to_group("enemies")
	
	if behaviour == Enums.EnemyBehaviour.BLINKY:
		add_to_group("blinky")
		
	agent.path_changed.connect(_on_path_changed)
	dynamic_rail.on_stop.connect(_on_path_completed)
	
	if start_recoverying == true:
		var current_rt = recovery_time
		_set_recovery_time(first_recovery_time)
		call_deferred('recovering')
		call_deferred('_set_recovery_time', current_rt)
	else:
		call_deferred('move')
	
func _physics_process(delta):
	agent.get_next_path_position()

func _set_recovery_time(time):
	recovery_time = time
	
func set_mode(mode: Enums.EnemyMode):
	_current_mode = mode
	
	if _current_mode == Enums.EnemyMode.FRIGHTENED or _current_mode == Enums.EnemyMode.HIT:
		_killable = true
	else:
		_killable = false
		
func get_mode():
	return _current_mode

func _start_move_timer():
	move_timer.one_shot = true
	move_timer.start(_get_best_refresh_rate())
	
func _start_frightened_timer():
	frightened_timer.one_shot = true
	frightened_timer.start(frightened_time)
		
func move():
	_stop_all_timers()
	var mode = calculate_move_mode()
	var previous_mode = _current_mode
	set_mode(mode)
	modulate_velocity(Enums.EnemySpeedMode.NORMAL)
	
	if (mode == Enums.EnemyMode.CHASE) or previous_mode != mode:
		update_path()
		
	_start_move_timer()
	
func frighten():
	
	if _current_mode == Enums.EnemyMode.CHASE \
		or _current_mode == Enums.EnemyMode.SCATTERED \
		or _current_mode == Enums.EnemyMode.FRIGHTENED:
		_stop_all_timers()
		set_mode(Enums.EnemyMode.FRIGHTENED)
		modulate_velocity(Enums.EnemySpeedMode.SLOW)
		update_path()
		_start_frightened_timer()

func calculate_move_mode() -> int:
	var time = int(Time.get_unix_time_from_system()) % chase_scatted_interval
	if time < chase_scatted_interval * chase_ratio:
		return Enums.EnemyMode.CHASE
		
	return Enums.EnemyMode.SCATTERED
			
func _get_random_target():
	var tilemap = dynamic_rail.get_parent().get_node("Maze")
	var rect: Rect2i = tilemap.get_used_rect()
	var x = _random_number_generator.randi_range(rect.position.x,rect.end.x)
	var y = _random_number_generator.randi_range(rect.position.y,rect.end.y)
	var random_pos = Vector2(x,y)
	return tilemap.map_to_local(random_pos)
	
func _get_fallback_target():
	var nest = dynamic_rail.get_parent().get_node_or_null("Nest")
	if nest == null:
		return _init_position
		
	var target = nest.get_recovery_target()
	return target
			
func _get_best_refresh_rate():
	if auto_refresh_rate or (_points_to_target != null and _points_to_target < 5):
		return (1 / dynamic_rail.speed_factor) - 0.1
	
	return dynamic_rail.speed_factor
			
func set_target(target):
	_target_changed = true
	agent.target_position = target
	
func get_target():
	return agent.target_position

func _on_path_completed() -> void:
	set_target(dynamic_rail.wagon_position())
	if _current_mode == Enums.EnemyMode.HIT:
		recovering()
	elif _current_mode == Enums.EnemyMode.LAUNCHING:
		dynamic_rail.jump = false
		move()
	elif _current_mode == Enums.EnemyMode.SCATTERED and _preparing_scattering == true:
		_preparing_scattering = false
		_push_path(_scattering_path)
	else:
		update_path()

func recovering():
	_stop_all_timers()
	set_mode(Enums.EnemyMode.RECOVERING)
	recovery_timer.one_shot = true
	recovery_timer.start(recovery_time)

func get_blinky():
	var blinky_array = get_tree().get_nodes_in_group("blinky")

	if(blinky_array.size() > 0):
		return blinky_array[0]
		
	return null
		
func update_path() -> void:
	
	var target = null

	match _current_mode:
		Enums.EnemyMode.CHASE:
			if behaviour == Enums.EnemyBehaviour.CLYDE:
				target = _get_random_target() # set behaviour
			elif behaviour == Enums.EnemyBehaviour.PINKY or behaviour == Enums.EnemyBehaviour.INKY:
				var user_position = _get_user_target()
				var user_offset_position = _get_user_offset_target()
				var my_position = dynamic_rail.wagon_position()
				var a = user_offset_position - user_position
				var b = user_offset_position - my_position
				if a.cross(b) != 0 or (a.normalized() == b.normalized() and a.length() < b.length()):
					target = user_offset_position
				else:
					target = user_position
					
				if behaviour == Enums.EnemyBehaviour.INKY:
					var blinky = get_blinky()
					
					if blinky != null:
						var blinky_position = blinky.dynamic_rail.wagon_position()
						var target_blinky_distance = target - blinky_position
						target = target + target_blinky_distance
			else:
				target = _get_user_target()
		Enums.EnemyMode.SCATTERED:
			if _scattering_path != null:
				_preparing_scattering = true
				target = _scattering_path[0]
			else:
				target = _get_random_target()
		Enums.EnemyMode.FRIGHTENED:
			target = _get_random_target()
		Enums.EnemyMode.HIT:
			target = _get_fallback_target()
		Enums.EnemyMode.LAUNCHING:
			target = _get_launch_target()
		_:
			return
	
	set_target(target)
	
func _push_path(path):
	dynamic_rail.clear_paths()	
	dynamic_rail.push_path(path)
	_points_to_target = path.size()
	dynamic_rail.make_rail()
		
func _on_path_changed() -> void:
	var path = agent.get_current_navigation_path()
		
	if debug_line:
		debug_line.points = path
		
	if _target_changed:
		_target_changed = false
		_push_path(path)
		
		if dynamic_rail.current_path_size() == 0:
			update_path()
	
func _get_user_target():
	var user = get_tree().get_nodes_in_group("users")[0]
	
	if user != null:
		return user.wagon_position()
		
	return Vector2.ZERO
	
func _get_user_offset_target():
	var user = get_tree().get_nodes_in_group("users")[0]
	
	if user != null:
		return user.offset_position()
		
	return Vector2.ZERO
	
func _get_launch_target():
	dynamic_rail.jump = true
	var nest = dynamic_rail.get_parent().get_node_or_null("Nest")

	if nest == null:
		return _get_user_target()
		
	var target = nest.get_launch_target()
	
	if target == null:
		return _get_user_target()
		
	return target

func hit():
	if !_killable:
		return
	_stop_all_timers()
	dynamic_rail.jump = true
	set_mode(Enums.EnemyMode.HIT)
	modulate_velocity(Enums.EnemySpeedMode.FAST)
	update_path()
	
func catched(character):
	var level = dynamic_rail.get_parent()
	
	if _killable:
		hit()
		if level.has_method("enemy_killed"):
			level.enemy_killed()
	else:
		if level.has_method("character_killed"):
			level.character_killed()

func modulate_velocity(mode: Enums.EnemySpeedMode):
	
	match mode:
		Enums.EnemySpeedMode.NORMAL:
			dynamic_rail.speed_factor = _init_speed_factor
		Enums.EnemySpeedMode.SLOW:
			dynamic_rail.speed_factor = _init_speed_factor / slow_factor
		Enums.EnemySpeedMode.FAST:
			dynamic_rail.speed_factor = _init_speed_factor * fast_factor

func _stop_all_timers():
	move_timer.stop()
	frightened_timer.stop()
	recovery_timer.stop()
	
func _on_move_timer_timeout():
	move()

func _on_frightened_timer_timeout():
	move()

func _on_recovery_timer_timeout():
	modulate_velocity(Enums.EnemySpeedMode.NORMAL)
	set_mode(Enums.EnemyMode.LAUNCHING)
	update_path()
