extends Node2D

@onready var dynamic_rail = get_parent().get_parent()
@onready var up_label = $Up
@onready var down_label = $Down
@onready var left_label = $Left
@onready var right_label = $Right

@export var show_label: bool = false

var up: bool = false
var down: bool = false
var left: bool = false
var right: bool = false
var clear: bool = false

var _directions: Array = []

func _ready():
	dynamic_rail.add_to_group("users")
	
func _clear_directions():
	_directions = []
	
func _push_direction(dir: Vector2):
	
	if _directions.find(dir) == -1:
		_directions.push_front(dir)
		
func _remove_direction(dir: Vector2):
	var f = _directions.find(dir)
	
	if f != -1:
		_directions.remove_at(f)

func _process(delta):
	var current_up = false
	var current_down = false
	var current_left = false
	var current_right = false
	var changed = false
	
	if Input.is_action_pressed("ui_up"):
		current_up = true
	
	if Input.is_action_pressed("ui_down"):
		current_down = true
		
	if Input.is_action_pressed("ui_left"):
		current_left = true
		
	if Input.is_action_pressed("ui_right"):
		current_right = true
		
	
	if current_up != up:
		changed = true
		if current_up == true:
			_push_direction(Vector2.UP)
		else:
			_remove_direction(Vector2.UP)
		
	if current_down != down:
		changed = true
		if current_down == true:
			_push_direction(Vector2.DOWN)
		else:
			_remove_direction(Vector2.DOWN)
		
	if current_left != left:
		changed = true
		if current_left == true:
			_push_direction(Vector2.LEFT)
		else:
			_remove_direction(Vector2.LEFT)
		
	if current_right != right:
		changed = true
		if current_right == true:
			_push_direction(Vector2.RIGHT)
		else:
			_remove_direction(Vector2.RIGHT)
		
	if changed:
		if !dynamic_rail.clear_paths():
			_clear_directions()
			current_up = false
			current_down = false
			current_left = false
			current_right = false
		else:
			for dir in _directions:
				dynamic_rail.push_direction(dir)
				
			dynamic_rail.make_rail()
		
	up = current_up
	down = current_down
	left = current_left
	right = current_right
	
	up_label.visible = current_up and show_label
	down_label.visible = current_down and show_label
	left_label.visible = current_left and show_label
	right_label.visible = current_right and show_label
