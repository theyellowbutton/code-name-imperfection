extends Control

@export var initial_lives = 3
@export var points_for_up = 5000
@export var points_for_coin = 10
@export var points_for_energizer = 40
@export var points_for_bonus = 200
@export var points_for_enemy = 100
@export var enable_hud: bool = false:
	get:
		return enable_hud
	set(value):
		enable_hud = value
		_enable_hud(enable_hud)

@onready var _hud_level = $HUD/Level
@onready var _hud_score = $HUD/Score
@onready var _hud_lives = $HUD/Lives
@onready var _hud_bonus = $HUD/Bonus
@onready var _hud = $HUD
var _collected_points_for_up = 0
var _score: int = 0:
	get:
		return _score
	set(value):
		_score = value
		_hud_score.text = str(_score)
		
var _lives = 0:
	get:
		return _lives
	set(value):
		_lives = value
		_hud_lives.text = "X " + str(_lives)
		
var _level = 0:
	get:
		return _level
	set(value):
		_level = value
		_hud_level.text = "Lv " + str(_level)
		
var _bonus = 0:
	get:
		return _bonus
	set(value):
		_bonus = value
		_hud_bonus.text = "X " + str(_bonus)

func _ready():
	reset()

func reset():
	_score = 0
	_collected_points_for_up = 0
	_lives = initial_lives
	_level = 1
	_bonus = 0

func _enable_hud(value):
	_hud.visible = value
	
func get_life() -> bool:
	if _lives > 0:
		_lives -= 1
		
	return _lives > 0
	
func next_level():
	_level += 1
	_bonus = 0
	
func calculate_and_add_points(on: Enums.CollectableType):
	var points = 0

	match on:
		Enums.CollectableType.coin:
			points = points_for_coin
		Enums.CollectableType.energizer:
			points = points_for_energizer
		Enums.CollectableType.bonus:
			points = points_for_bonus
			_bonus += 1
		Enums.CollectableType.enemy:
			points = points_for_enemy
	
	add_points(points)

func add_points(points: int):
	_collected_points_for_up += points
	_score += points
	var extra_lives = _collected_points_for_up / points_for_up
	if extra_lives > 0:
		_collected_points_for_up -= extra_lives * points_for_up
		_lives += extra_lives
	
func _process(delta):
	pass
