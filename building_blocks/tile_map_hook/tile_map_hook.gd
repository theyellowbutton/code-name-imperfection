extends Node2D

class_name TileMapHook

@export var tilemap: TileMap = null;
@export var align_parent = false

var tile_size: Vector2:
	get:
		if tilemap != null:
				return Vector2(tilemap.tile_set.tile_size.x, tilemap.tile_set.tile_size.y)
		return Vector2.ZERO
		
var _init_position

func _ready():
	
	if tilemap == null:
		if get_tree().get_current_scene().get_node_or_null("Level"):
			tilemap = get_tree().get_current_scene().get_node("Level").get_node_or_null("Maze")
		else:
			tilemap = get_tree().get_current_scene().get_node_or_null("Maze")
		
	_init_position = get_parent().position
	
	if tilemap == null or !align_parent:
		return
		
	var new_position = hook_the_corner(_init_position)
	
	if new_position == Vector2(-1,-1):
		get_parent().queue_free()
		return
		
	get_parent().position = new_position
	
func hook_the_corner(point: Vector2, jump: bool = false) -> Vector2:
	var cell = tilemap.local_to_map(point)
	var center_position = tilemap.map_to_local(cell)
	var tile_data: TileData = _get_tile_data(0, cell)
	var tile_corners = _get_tile_corners(center_position, tile_size)
	
	if tile_data == null:
		return Vector2(-1,-1)
		
	var distance_to_nw = point.distance_to(tile_corners.north_west) \
		if tile_data.get_custom_data("nw_corner_hook") or (tile_data.get_custom_data("nw_corner_hook_jump") and jump) \
		else INF
	var distance_to_ne = point.distance_to(tile_corners.north_east) \
		if tile_data.get_custom_data("ne_corner_hook") or (tile_data.get_custom_data("ne_corner_hook_jump") and jump) \
		else INF
	var distance_to_sw = point.distance_to(tile_corners.south_west) \
		if tile_data.get_custom_data("sw_corner_hook") or (tile_data.get_custom_data("sw_corner_hook_jump") and jump) \
		else INF
	var distance_to_se = point.distance_to(tile_corners.south_east)\
		if tile_data.get_custom_data("se_corner_hook") or (tile_data.get_custom_data("se_corner_hook_jump") and jump) \
		else INF
	var min_distance = min(distance_to_nw, distance_to_ne, distance_to_sw, distance_to_se)
	var new_position = Vector2(-1,-1)
	
	if min_distance == INF:
		return new_position
	
	if distance_to_nw == min_distance:
		new_position = tile_corners.north_west
	elif distance_to_ne == min_distance:
		new_position = tile_corners.north_east
	elif distance_to_sw == min_distance:
		new_position = tile_corners.south_west
	elif distance_to_se == min_distance:
		new_position = tile_corners.south_east
	
	return new_position
	
	
func _get_tile_data(layer, cell) -> TileData:
	var tile_set_source_id = tilemap.get_cell_source_id(layer, cell, false)
	
	if tile_set_source_id == -1:
		return null
		
	var tile_set_source = tilemap.tile_set.get_source(tile_set_source_id)
	if tile_set_source:
		return tile_set_source.get_tile_data(tilemap.get_cell_atlas_coords(layer, cell, false), 0)
	return null
	
func _get_tile_corners(center_position, size) -> Dictionary:
	var half_x = size.x / 2
	var half_y = size.y / 2
	return {
		"north_west": Vector2(center_position.x - half_x, center_position.y - half_y),
		"north_east": Vector2(center_position.x + half_x, center_position.y - half_y),
		"south_west": Vector2(center_position.x - half_x, center_position.y + half_y),
		"south_east": Vector2(center_position.x + half_x, center_position.y + half_y),
	}
