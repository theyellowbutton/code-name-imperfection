extends Node2D

@onready var _paths = $Paths
@onready var _tmhook = $TileMapHook
@onready var _spawned = $Spawned
@onready var _random_spawner = $RandomSpawner

var _spawnables: Node2D = null
var _random = null

var _spawned_dict = {}
var _collected = []
var _random_spawned = 0

@export var character: Node2D = null
@export var character_check_threshold = 64
@export var enable_random_spawn = true
@export var random_spawn_min_interval = 10
@export var random_spawn_max_interval = 20
@export var random_collections_to_stop_spawn = 1
@export var random_expires_after = 4
@export var collectables_in_a_segment = 1:
	set(value):
		if value < 0:
			value = 0
			
		collectables_in_a_segment = value
	
var collectables_left = 0
var collected = 0
var total_collected = 0
var collectables = 0
var random_collected = 0
	
signal all_collected
signal status_updated
signal on_collected(what: Enums.CollectableType)
		
func _ready():
	randomize()
	var _spawnables = get_node_or_null("Spawnables")
	
	if _spawnables != null:
		var item = _spawnables.get_child(0)
		var single = _spawnables.get_child(1)
		_random = _spawnables.get_child(2)
		_collected = []
		
		if item != null:
			item.visible = false
			
		if single != null:
			single.visible = false
			
		if _random != null:
			_random.visible = false
		
		for child in _spawned.get_children():
			child.queue_free()
		
		for path in _paths.get_children():
			var p = PathUtility.convert_curve_to_path(path.get_curve(), _tmhook)
			var adapted_path = PathUtility.adapt_path(p, _tmhook)
			
			if adapted_path.size() > 1 and item != null:
				_spawn_collectables_in_a_row(adapted_path, item)
			elif adapted_path.size() == 1 and single != null:
				_spawn_single_collectable(adapted_path[0], single)
			
	_update_status()
	_start_random_spawner()
	
func _start_random_spawner():
	
	if collectables_left == 0 and collectables > 0:
		return 
		
	if enable_random_spawn and collectables_album.random_collected < random_collections_to_stop_spawn:
		var next_random_spawn = randi() % random_spawn_max_interval \
			+ random_spawn_min_interval
		_random_spawner.one_shot = true
		_random_spawner.wait_time = next_random_spawn
		_random_spawner.start()
	
func _spawn_single_collectable(point: Vector2, model: Node2D):
	spawn(point, model, "single", true, false, true)
	
func _spawn_collectables_in_a_row(path: PackedVector2Array, model: Node2D):
	var i = 0
		
	while i < path.size():
		var no_increment = false
		var increment = (path[i+1] - path[i]) / (collectables_in_a_segment + 1)  \
			if i + 1 < path.size() \
			else  Vector2.ZERO
			
		if increment == Vector2.ZERO:
			no_increment = true
			
		for j in range(0, collectables_in_a_segment * int(!no_increment) + 1):
			var pos = path[i] + j * increment
			spawn(pos, model, "items", false, false, true)
		
		i += 1
	
func spawn(pos: Vector2, model: Node2D, group = "items", override = false, remove = false, check_in_album = false) -> bool:
	
	if model == null:
		return false
		
	if character.wagon_position().distance_to(pos) <= character_check_threshold:
		return false
		
	if override and pos in _spawned_dict:
		_spawned_dict[pos].queue_free()
		_spawned_dict.erase(pos)
		
	if pos not in _spawned_dict and (check_in_album == false || \
		(check_in_album == true and pos not in collectables_album.collected_dict)):
		var instance = model.duplicate()
		instance.position = pos
		_spawned_dict[instance.position] = instance
		instance.connect("collected", _on_collected)
		instance.connect("expired", _on_expired)
		instance.visible = true
		instance.add_to_group(group)
		
		if remove:
			instance.expires_after = random_expires_after

		_spawned.call_deferred("add_child", instance)
		return true
	
	return false
	
func spawn_randomly():
	if _collected.size() > 0:
		var random_position = randi() % _collected.size()

		if _random != null:
			_random_spawned += int(spawn(_collected[random_position], _random, 'random', false, true))
		
	_start_random_spawner()
	
func _on_collected(pos, groups):	
	_spawned_dict.erase(pos)
	_collected.push_back(pos)
	collectables_album.collected_dict[pos] = true
	
	if groups.find(StringName("random"), 0) != -1:
		collectables_album.random_collected += 1
		_random_spawned -= 1
		
	if _spawned_dict.size() - _random_spawned == 0:
		emit_signal("all_collected")
		
	_update_status()
	
	var what = Enums.CollectableType.bonus
	
	if groups.find(StringName("items"), 0) != -1:
		what = Enums.CollectableType.coin
	elif groups.find(StringName("single"), 0) != -1:
		what = Enums.CollectableType.energizer
		
	emit_signal("on_collected", what)
	
func _update_status():
	collectables_left = _spawned_dict.size() - _random_spawned
	total_collected = _collected.size()
	collected = total_collected - collectables_album.random_collected
	collectables = collectables_left + collected
	random_collected = collectables_album.random_collected
	emit_signal("status_updated")
	
func _on_expired(position, groups):
	_spawned_dict.erase(position)
	_random_spawned -= 1
