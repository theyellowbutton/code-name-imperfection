extends Node2D

signal on_collected(what: Enums.CollectableType)
signal game_over(won: bool)

func _ready():
	set_process_mode(Node.PROCESS_MODE_DISABLED)
	var collectable_handler = get_node_or_null("CollectablesHandler")
	
	if collectable_handler != null:
		collectable_handler.connect("on_collected", _collectables_handler_on_collected)
		collectable_handler.connect("all_collected", _collectables_handler_all_collected)
	
func character_killed():
	emit_signal("game_over", false)
	
func _collectables_handler_on_collected(what: Enums.CollectableType):
	emit_signal("on_collected", what)
	
func enemy_killed():
	emit_signal("on_collected", Enums.CollectableType.enemy)
	
func _collectables_handler_all_collected():
	emit_signal("game_over", true)
	
func get_camera():
	return get_node_or_null("Camera2D")

func start():
	set_process_mode(Node.PROCESS_MODE_INHERIT)
	
	
