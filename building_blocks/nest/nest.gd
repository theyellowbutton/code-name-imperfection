extends Node2D

@onready var _recovery_curve: Path2D = null
@onready var _launch_curve: Path2D = null
@onready var _tmhook: TileMapHook = $TileMapHook

var _recovery_path: Array = []
var _launch_path: Array = []
var _indexes = {}

func _ready():
	_indexes.recovery_index = 0
	_indexes.launch_index = 0
	_recovery_curve = get_node_or_null("RecoveryPath")
	_launch_curve = get_node_or_null("LaunchPath")
	var _r_curve = _recovery_curve.get_curve()
	var _l_curve = _launch_curve.get_curve()
	
	if _recovery_curve != null and _r_curve != null:
		var path = PathUtility.convert_curve_to_path(_r_curve, _tmhook)
		_recovery_path = PathUtility.adapt_path(path, _tmhook)
		
	if _launch_curve != null and _l_curve != null:
		var path = PathUtility.convert_curve_to_path(_l_curve, _tmhook)
		_launch_path = PathUtility.adapt_path(path, _tmhook)
		
func _get_next_point(pool: Array, index: String):
	
	if pool.size() == 0:
		return null
		
	var target = pool[_indexes[index]]
	_indexes[index] += 1
	
	if _indexes[index] >= pool.size():
		_indexes[index] = 0
		
	return target
	
func get_recovery_target():
	return _get_next_point(_recovery_path, "recovery_index")
	
func get_launch_target():
	return _get_next_point(_launch_path, "launch_index")
