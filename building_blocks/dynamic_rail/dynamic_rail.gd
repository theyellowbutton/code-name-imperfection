extends Path2D
class_name DynamicRail


@onready var tmhook = $TileMapHook
@onready var line = $RailLine
@onready var rail = $Rail
@onready var wagon = $Rail/Wagon
@onready var passenger = $Passenger
@onready var placeholder = $Rail/Wagon/Placeholder

@export var go_ahead = false
@export var speed_factor: float = 1
@export var draw_rail_line: bool = false
@export var draw_rail_line_points_value: bool = false
@export var jump = false
@export var rail_line_color: Color = Color(1,1,1)
@export var teletransport_cooldown: int = 2

signal on_stop
signal on_start

var _paths: Array = []
var _adapted_paths: Array = []
var _current_path: PackedVector2Array = []
var _working_path: PackedVector2Array = []
var _progress_direction = 1
var _stopped = true
var _teletransport_position: Vector2 = Vector2.ZERO
var _teletransport_cooldown_count: int = 0

func _ready():

	if passenger != null:
		for child in wagon.get_children():
			child.queue_free()
			
		self.remove_child(passenger)
		wagon.add_child(passenger)
		passenger.owner = self
	elif placeholder != null:
		placeholder.visible = true
		
	var init_curve = get_curve()

	if self.position != Vector2.ZERO:
		
		if init_curve != null:
			var init_path = PathUtility.convert_curve_to_path(get_curve(), tmhook)
			var path = _translate_path(init_path, self.position)
			set_curve(PathUtility.convert_path_to_curve(path))
			
		rail.position = self.position
		self.position = Vector2.ZERO

	init_curve = get_curve()
	rail.position = tmhook.hook_the_corner(rail.position, jump)
	
	if init_curve != null:
		var init_path = PathUtility.convert_curve_to_path(get_curve(), tmhook)
		clear_paths()
		push_path(init_path)
	make_rail()

# 1 - same path
# 0 - not same
# 1 - same reversed
func _detect_same_path(path: PackedVector2Array) -> int:
	
	if(path.size() == 0 or _current_path.size() == 0):
		if(path.size() == _current_path.size()):
			return 1
		return 0
		
	if(path.size() == 1):
		if path[0] == _current_path[0]:
			return 1
		return 0
	
	if(path.size() != _current_path.size()):
		return 0
		
	if path == _current_path:
		return _progress_direction
	
	for i in range(0, path.size()):
		if path[i] != _current_path[-i -1]:
			return 0
			
	return -1 * _progress_direction

func _get_current_path_index() -> int:
	return int(rail.progress_ratio / (_current_path.size() - 1))
	
func teletransport(teletransport_position):
	
	if _teletransport_cooldown_count == 0:
		_teletransport_cooldown_count = teletransport_cooldown
		_teletransport_position = teletransport_position
		
func cancel_teletransport():
	_teletransport_cooldown_count = 0
	_teletransport_position = Vector2.ZERO
	
func _adapt_path(path: PackedVector2Array) -> PackedVector2Array:
	
	if go_ahead and rail.progress_ratio != 1 and rail.progress_ratio != 0:
		var index = _get_current_path_index()
		var current_path_start = _current_path[index]
		var current_path_end = _current_path[index + 1]
		var path_to_adapt_start = path[0]
		var path_to_adapt_next = path[1]
		var path_first_direction = (path_to_adapt_next - path_to_adapt_start).normalized()
		var line_test = (current_path_end - current_path_start).normalized().dot(path_first_direction)
		
		if(abs(line_test) != 1):
			return PackedVector2Array()
	
	return PathUtility.adapt_path(path, tmhook, jump)

func _translate_path(path: PackedVector2Array, offset: Vector2) -> PackedVector2Array:
	var my_transform = Transform2D.IDENTITY
	my_transform.origin = offset
	return path * my_transform.inverse()

func current_path_size():
	return _current_path.size()
				
func _translate_paths():
	var offset = _current_path[-1] - _current_path[0]
	
	if _progress_direction == -1:
		offset = _current_path[0] - _current_path[-1]
		
	if _teletransport_position != Vector2.ZERO:

		if _progress_direction == -1:
			cancel_teletransport()
		else:
			_teletransport_cooldown_count = teletransport_cooldown
			offset = _teletransport_position - _current_path[0]
			_teletransport_position = Vector2.ZERO
			_working_path = _adapt_path(_translate_path(_working_path, offset))
			_paths = _paths.map(func(e): return _translate_path(e, offset))
			_adapted_paths = _paths.map(func(e): return _adapt_path(e))
			return
		
	_working_path = _adapt_path(_translate_path(_working_path, offset))
	_paths = _paths.map(func(e): return _translate_path(e, offset))
	_adapted_paths = _paths.map(func(e): return _adapt_path(e))
	
func is_stopped():
	return _stopped
	
func clear_paths():
	
	if _teletransport_cooldown_count > 0:
		return false
		
	_paths.clear()
	_adapted_paths.clear()
	return true
	
func push_path(path: PackedVector2Array):	
	_paths.push_back(path)
	
func push_direction(v: Vector2):
	
	if _teletransport_cooldown_count > 0:
		return
	
	if [0,1].find(int(abs(v.dot(Vector2.UP)))) == -1:
		return
	
	var start = null
	var _end = null
	var next_increment = v * (tmhook.tile_size.x if v.dot(Vector2.UP) == 0 else tmhook.tile_size.y)
	
	if _current_path.size() == 0:
		start = wagon_position()
		_end = start + next_increment
	elif _current_path.size() == 1:
		start = _current_path[0]
		_end = start + next_increment
	else:
		var index = 0
		index = _get_current_path_index()
		var a = _current_path[index]
		var b = _current_path[index + 1] if index < _current_path.size() \
			else _current_path[index]
		var ab_orientation = (b - a).normalized().dot(v)
		
		if ab_orientation == 1:
			start = a
		elif ab_orientation == -1:
			start = b
		elif _progress_direction == -1:
			start = b
		else:
			start = a

	push_path([start, start + next_increment])
	
func wagon_position():
	return wagon.global_position
	
func offset_position():
	var wp = wagon_position()
	if _working_path.size() <= 1:
		return wp
		
	var direction = _working_path[1] - _working_path[0]	
	var dir_path = range(4).map(func(num): return wp + (direction * (num + 1)))
	var adapted = PathUtility.adapt_path(dir_path, tmhook, jump)
	return adapted[-1]

func _set_current_path(path: PackedVector2Array):
	_clear_rail_line()
	_current_path = path
	if !go_ahead:
		if(path.size() > 0 and path[0] != wagon.global_position):
			var first_step = (path[1] - path[0])
			var first_new_step = (path[1] - wagon.global_position)
			if first_new_step.normalized().dot(first_step.normalized()) == 1 and \
				first_new_step.length() <= first_step.length():
				path.remove_at(0)
			path.insert(0, wagon.global_position)
	set_curve(PathUtility.convert_path_to_curve(path))
	_progress_direction = 1
	rail.progress_ratio = 0
	
	if path.size() < 2:
		_stop()
		
	if draw_rail_line:
		_draw_rail_line()
	
func _set_working_path():
	var wp = _current_path.duplicate()
	
	if _progress_direction == -1:
		wp.reverse()
		
	_working_path = wp

func _adapt_paths():
	for path in _paths:
		var adapted_path = _adapt_path(path)
		_adapted_paths.push_back(adapted_path)
		
func make_rail():
	_adapt_paths()
	
	if _adapted_paths.size() == 0 and _working_path.size() == 0:
		_stop()
		return
	
	var selected_apath = _working_path
	_adapted_paths.append(_working_path)
	
	for apath in _adapted_paths:

		if apath.size() > 1:
			_start()
			var detect_same_or_reversed_path = _detect_same_path(apath)
			
			if detect_same_or_reversed_path != 0:
				if detect_same_or_reversed_path == -1:
					_progress_direction *= detect_same_or_reversed_path
				_set_working_path()
				return
			
			selected_apath = apath
			break
		
	_set_current_path(selected_apath)
	_set_working_path()

	
func _draw_rail_line():
	for point in _current_path:
		_add_point_to_rail_line(point)
					
func _clear_rail_line():
	line.clear_points()

	for child in line.get_children():
		child.queue_free()

func _add_point_to_rail_line(point):
	line.add_point(point)
	_draw_rail_line_square(point, line.default_color)
	
func _draw_rail_line_square(center, color):
	var square = Polygon2D.new()
	var square_size = 16
	square.polygon = PackedVector2Array([
		Vector2(-square_size, -square_size),
		Vector2(square_size, -square_size),
		Vector2(square_size, square_size),
		Vector2(-square_size, square_size)
	])
	square.color = rail_line_color
	square.position = center
	line.add_child(square)
	
	if draw_rail_line_points_value:
		var label = Label.new()
		label.text = "" + str(center)
		var labelSetting = LabelSettings.new()
		labelSetting.font_size = 20
		labelSetting.font_color = rail_line_color
		label.set_label_settings(labelSetting)
		line.add_child(label)
		label.position = center * Transform2D(0, Vector2(20,20)).inverse()

func _stop():
	set_physics_process(false)
	if _stopped == false:
		clear_paths()
		_working_path = []
		_stopped = true
		emit_signal("on_stop")
	
func _start():
	set_physics_process(true)
	if _stopped == true:
		_stopped = false
		emit_signal("on_start")
		
func _physics_process(delta):
	var stop = false;
	var increment_offset = 0
	var max_increment = delta * min(tmhook.tile_size.x, tmhook.tile_size.y) \
		* speed_factor * _progress_direction
	
	var current_progress = rail.progress
	rail.progress += max_increment
	
	increment_offset = (rail.progress - current_progress)
	if ((rail.progress_ratio == 1 and _progress_direction == 1) or \
		(rail.progress_ratio == 0 and _progress_direction == -1)):
			if go_ahead:
				_translate_paths()
				
				if _teletransport_cooldown_count > 0:
					_teletransport_cooldown_count -= 1
		
				make_rail()
			else:
				stop = true
	
	var remainder_increment = snapped(max_increment - increment_offset, 0.00001) 

	rail.progress += remainder_increment
	if stop and _stopped == false:
		_stop()


func refresh_target():
	pass # Replace with function body.
