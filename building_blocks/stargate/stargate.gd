extends Area2D

@onready var _tmhook: TileMapHook = $TileMapHook
var _marker = null

func _ready():

	_marker = get_node_or_null("Marker2D")
	
	if _marker == null:
		queue_free()
		return
		
	var marker_position = _marker.global_position
	global_position = _tmhook.hook_the_corner(global_position)
	_marker.global_position = _tmhook.hook_the_corner(marker_position)

func _on_body_entered(body):
	var dynamic_rail = body.get_parent().get_parent().get_parent()
	dynamic_rail.teletransport(_marker.global_position)
