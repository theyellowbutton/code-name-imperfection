extends Area2D
class_name Collectable
@export var free = true
@export var expires_after = -1

@onready var timer = $Timer
@onready var placeholder = $Placeholder
@onready var pcs = $PlaceholderCollisionShape2D

signal collected(position, groups)
signal expired(position, groups)

func _count_collision_shapes() -> int:
	
	var count_collision_shapes = func(sum: int, node):
		if node is CollisionShape2D:
			sum += 1
			
		return sum
		
	return get_children().reduce(count_collision_shapes, 0)

func _ready():
	
	var collision_shapes_count = _count_collision_shapes()
	
	if get_children().size() - collision_shapes_count - int(get_node_or_null("CollectableBehaviour") != null) > 2:
		placeholder.queue_free()
	
	if  collision_shapes_count > 1: # there is only one CollisionShape By Default
		pcs.queue_free()

	if expires_after >= 0:
		timer.one_shot = true
		timer.wait_time = expires_after
		timer.start()
	
func _collected(body):
	var behaviour = get_node_or_null("CollectableBehaviour")
	if behaviour != null and behaviour.has_method("collected"):
		behaviour.collected()
	
	emit_signal("collected", position, get_groups())
	
	if free:
		queue_free()

func _on_timer_timeout():
	emit_signal("expired", position, get_groups())
	if free:
		queue_free()
